//#region import libs
const express = require('express');
const app = express();
const environment = require('dotenv');
const path = require('path');
const session = require('express-session');
const siteRouter = require('./routes/site');
const apiRouter = require('./routes/api');
const Middleware = require('./controllers/Middleware');
const cookieParser = require('cookie-parser');
const { Server } = require('socket.io');
const { Conversation, User } = require('./models');
//#endregion

//#region somes config
environment.config();

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
  })
);
app.use(cookieParser());
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

const server = app.listen(process.env.PORT || 3000, () => {
  console.log(`Listening on http://localhost:${server.address().port}`);
});

app.use('/api', apiRouter);
app.use('/', Middleware.auth, siteRouter);
//#endregion

const io = new Server(server);

let users = [];

const addUser = (user, socketId) => {
  !users.some((u) => u.user.id === user.id) && users.push({ user, socketId });
};

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((u) => u.user.id === userId);
};

io.on('connection', (socket) => {
  console.log('A user connected!');

  socket.on('addUser', async (userId) => {
    const user = await User.findOne({ where: { id: userId } });
    addUser(
      { id: user.id, fullName: user.fullName, avatar: user.avatar },
      socket.id
    );
    io.emit('getUsers', users);
  });

  socket.on('sendMessage', async (message) => {
    const conversation = await Conversation.findOne({
      where: { id: message.conversationId },
    });

    const receivers = conversation.members.filter(
      (memberId) => memberId !== message.senderId
    );

    if (receivers < 2) {
      const receiver = getUser(receivers[0]);
      if (receiver) {
        io.to(receiver.socketId).emit('getMessage', message);
      }
    } else {
      receivers.forEach((receiverId) => {
        const receiver = getUser(receiverId);
        if (receiver) {
          io.to(receiver.socketId).emit('getMessage', message);
        }
      });
    }
  });

  socket.on('sendTyping', async ({ user, conversationId }) => {
    const conversation = await Conversation.findOne({
      where: { id: conversationId },
    });

    const receivers = conversation.members.filter(
      (memberId) => memberId !== user.id
    );

    if (receivers < 2) {
      const receiver = getUser(receivers[0]);
      if (receiver) {
        io.to(receiver.socketId).emit('getTyping', {
          user,
          conversationId,
        });
      }
    } else {
      receivers.forEach((receiverId) => {
        const receiver = getUser(receiverId);
        if (receiver) {
          io.to(receiver.socketId).emit('getTyping', {
            user,
            conversationId,
          });
        }
      });
    }
  });

  socket.on('sendOffTyping', async ({ user, conversationId }) => {
    const conversation = await Conversation.findOne({
      where: { id: conversationId },
    });

    const receivers = conversation.members.filter(
      (memberId) => memberId !== user.id
    );

    if (receivers < 2) {
      const receiver = getUser(receivers[0]);
      if (receiver) {
        io.to(receiver.socketId).emit('getOffTyping', { user, conversationId });
      }
    } else {
      receivers.forEach((receiverId) => {
        const receiver = getUser(receiverId);
        if (receiver) {
          io.to(receiver.socketId).emit('getOffTyping', {
            user,
            conversationId,
          });
        }
      });
    }
  });

  socket.on('disconnect', () => {
    console.log('User disconnected');
    removeUser(socket.id);
    io.emit('getUsers', users);
  });
});
