'use strict';

module.exports = {
  async up(queryInterface) {
    await queryInterface.addConstraint('Messages', {
      fields: ['conversationId'],
      type: 'foreign key',
      name: 'message_conversation_association',
      references: {
        table: 'Conversations',
        field: 'id',
      },
    });

    await queryInterface.addConstraint('Messages', {
      fields: ['senderId'],
      type: 'foreign key',
      name: 'message_user_association',
      references: {
        table: 'Users',
        field: 'id',
      },
    });
  },

  async down(queryInterface) {
    await queryInterface.removeConstraint(
      'Messages',
      'message_conversation_association'
    );

    await queryInterface.removeConstraint(
      'Messages',
      'message_user_association'
    );
  },
};
