class SiteController {
  home(req, res) {
    if (!req.session.hasAuthenticated) {
      return res.redirect('/login');
    }

    res.render('index', { user: req.session.user });
  }

  login(req, res) {
    if (req.session.hasAuthenticated) {
      return res.redirect('/');
    }

    res.render('login');
  }

  logout(req, res) {
    // Case: Authenticated
    if (req.session.hasAuthenticated) {
      return req.session.destroy(() => {
        res.clearCookie('accessToken').redirect('/');
      });
    }

    res.redirect('/');
  }
}

module.exports = new SiteController();
