const { User } = require('../models');
const bcrypt = require('bcrypt');
const ResponseData = require('../utils/ResponseData');
const EnumStatus = require('../utils/EnumStatus');
const jwt = require('jsonwebtoken');

class AuthController {
  async login(req, res) {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });
    // Case: Is not a valid user
    if (!user) {
      return res
        .status(404)
        .json(
          new ResponseData(
            EnumStatus.NOT_FOUND,
            'This email does not match any account!'
          )
        );
    }
    // Check password
    const correct = await bcrypt.compare(password, user.password);

    // Case: Wrong password
    if (!correct) {
      return res
        .status(401)
        .json(new ResponseData(EnumStatus.ERROR, 'Wrong password!'));
    }

    // Case: Right password
    jwt.sign(
      {
        id: user.id,
        fullName: user.fullName,
        email: user.email,
        avatar: user.avatar,
      },
      process.env.SECRET_KEY,
      (err, token) => {
        res
          .status(200)
          .cookie('accessToken', token, {
            expires: new Date(Date.now() + 2 * 3600000),
            httpOnly: true,
          })
          .json(new ResponseData(EnumStatus.SUCCESS, 'Login successfully!'));
      }
    );
  }
}

module.exports = new AuthController();
