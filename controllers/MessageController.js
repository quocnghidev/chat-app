const { Message, User, sequelize, Sequelize } = require('../models');
const ResponseData = require('../utils/ResponseData');
const EnumStatus = require('../utils/EnumStatus');

class MessageController {
  async add(req, res) {
    const { conversationId, senderId, text } = req.body;
    try {
      // Case: add new message successful
      const newMessage = await Message.create({
        conversationId,
        senderId,
        text,
      });

      const message = await Message.findOne({
        where: {
          id: newMessage.id,
        },
        include: [
          {
            model: User,
            as: 'sender',
            attributes: ['id', 'fullName', 'avatar'],
          },
        ],
      });

      res
        .status(200)
        .json(
          new ResponseData(
            EnumStatus.SUCCESS,
            'add new message successfully!',
            message
          )
        );
    } catch (error) {
      // Case: Failed to add new message
      res
        .status(500)
        .json(new ResponseData(EnumStatus.ERROR, 'Can not add new message!'));
    }
  }

  async getLatest(req, res) {
    const conversationId = req.params.conversationId;
    try {
      const message = await Message.findOne({
        where: {
          conversationId: conversationId,
        },
        order: [['createdAt', 'DESC']],
        include: [
          {
            model: User,
            as: 'sender',
            attributes: ['id', 'fullName', 'avatar'],
          },
        ],
      });
      res
        .status(200)
        .json(
          new ResponseData(
            EnumStatus.SUCCESS,
            'Get latest message successfully!',
            message
          )
        );
    } catch (error) {
      // Case: Failed to excute query
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, 'Can not handle this request!')
        );
    }
  }

  async getAll(req, res) {
    const conversationId = req.params.conversationId;
    try {
      const allMessages = await Message.findAll({
        where: {
          conversationId: conversationId,
        },
        include: [
          {
            model: User,
            as: 'sender',
            attributes: ['id', 'fullName', 'avatar'],
          },
        ],
      });

      allMessages.length
        ? res
            .status(200)
            .json(
              new ResponseData(
                EnumStatus.SUCCESS,
                'Get all messages successfully!',
                allMessages
              )
            )
        : res
            .status(200)
            .json(
              new ResponseData(
                EnumStatus.EMPTY,
                'No results for this request!',
                allMessages
              )
            );
    } catch (error) {
      // Case: Failed to excute query
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, 'Can not handle this request!')
        );
    }
  }
}

module.exports = new MessageController();
