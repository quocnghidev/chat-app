const { User, Sequelize } = require('../models');
const ResponseData = require('../utils/ResponseData');
const EnumStatus = require('../utils/EnumStatus');

class UserController {
  async getInfo(req, res) {
    const userId = req.params.userId;
    try {
      const userIds = userId.split(',');

      if (userIds.length < 2) {
        const user = await User.findOne({
          where: {
            id: userId,
          },
        });
        return res.status(200).json(
          new ResponseData(
            EnumStatus.SUCCESS,
            'Get user information successfully!',
            {
              id: user.id,
              fullName: user.fullName,
              email: user.email,
              avatar: user.avatar,
            }
          )
        );
      }

      const users = await User.findAll({
        where: {
          id: {
            [Sequelize.Op.or]: userIds,
          },
        },
      });

      res.status(200).json(
        new ResponseData(
          EnumStatus.SUCCESS,
          'Get user information successfully!',
          users.map((user) => {
            return {
              id: user.id,
              fullName: user.fullName,
              email: user.email,
              avatar: user.avatar,
            };
          })
        )
      );
    } catch (error) {
      // Case: Failed to excute query
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, 'Can not handle this request!')
        );
    }
  }
}

module.exports = new UserController();
