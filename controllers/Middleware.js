const jwt = require('jsonwebtoken');

class Middleware {
  static auth(req, res, next) {
    if (!req.cookies.accessToken) {
      req.session.hasAuthenticated = false;
      return next();
    }
    try {
      const user = jwt.verify(req.cookies.accessToken, process.env.SECRET_KEY);
      req.session.hasAuthenticated = true;
      req.session.user = {
        id: user.id,
        fullName: user.fullName,
        email: user.email,
        avatar: user.avatar,
      };
      next();
    } catch (error) {
      req.session.hasAuthenticated = false;
      next();
    }
  }
}

module.exports = Middleware;
