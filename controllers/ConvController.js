const { Conversation, Sequelize } = require('../models');
const ResponseData = require('../utils/ResponseData');
const EnumStatus = require('../utils/EnumStatus');

class ConvController {
  async create(req, res) {
    const { membersId } = req.body;
    let members = JSON.parse(membersId);
    members = members.map((member) => parseInt(member));
    try {
      // Case: Create conversation successful
      const savedConversation = await Conversation.create({
        members: members,
      });
      res
        .status(200)
        .json(
          new ResponseData(
            EnumStatus.SUCCESS,
            'Create conversation successfully!',
            savedConversation
          )
        );
    } catch (error) {
      // Case: Failed to create conversation
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, 'Can not create conversation!')
        );
    }
  }

  async getAll(req, res) {
    const userId = req.params.userId;
    try {
      const allConversations = await Conversation.findAll({
        where: {
          members: { [Sequelize.Op.like]: `%${userId}%` },
        },
      });

      allConversations.length
        ? res
            .status(200)
            .json(
              new ResponseData(
                EnumStatus.SUCCESS,
                'Get conversations successfully!',
                allConversations
              )
            )
        : res
            .status(200)
            .json(
              new ResponseData(
                EnumStatus.EMPTY,
                'No results for this request!',
                allConversations
              )
            );
    } catch (error) {
      // Case: Failed to excute query
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, 'Can not handle this request!')
        );
    }
  }
}

module.exports = new ConvController();
