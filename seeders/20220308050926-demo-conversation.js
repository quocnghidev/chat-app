'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Conversations', [
      {
        members: '[1, 2]',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        members: '[1, 2, 3]',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        members: '[1, 3]',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        members: '[2, 3]',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Conversations', null, {});
  },
};
