'use strict';

const bcrypt = require('bcrypt');
const jamRounds = parseInt(process.env.JAM_ROUNDS);

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [
      {
        fullName: 'Lã Quốc Nghị',
        email: 'quocnghidev@gmail.com',
        password: await bcrypt.hash('quocnghidev', jamRounds),
        avatar: 'img/quocnghidev.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        fullName: 'Cao Hoài Nam',
        email: 'caonam123@gmail.com',
        password: await bcrypt.hash('caonam123', jamRounds),
        avatar: 'img/caonam123.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        fullName: 'Nguyễn Đức Long',
        email: 'longcua2k@gmail.com',
        password: await bcrypt.hash('longcua2k', jamRounds),
        avatar: 'img/longcua2k.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  },
};
