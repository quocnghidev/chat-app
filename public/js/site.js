/* global Swal */
(function ($, window, document) {
  $(function () {
    // The DOM is ready!
    const loginForm = $('#login-form');

    // Submit Login form handler
    loginForm.on('submit', function (event) {
      event.preventDefault();
      const body = getFormBody($(this));
      login(body)
        .done(function (res) {
          Swal.fire({
            title: res.status,
            text: res.message,
            icon: 'success',
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = '/';
            }
          });
        })
        .fail(function (res) {
          handleError(res);
        });
    });

    // Get body from <form>
    function getFormBody(form) {
      const body = {};
      const fields = form.serializeArray();
      fields.forEach((field) => {
        body[field.name] = field.value;
      });
      return body;
    }

    // Ajax call [POST] /api/login
    function login(body) {
      return $.ajax({
        type: 'POST',
        url: '/api/login',
        data: body,
      });
    }

    // Show message when catch error
    function handleError(res) {
      const error = res.responseJSON;
      Swal.fire({
        title: error.status,
        text: error.message,
        icon: 'error',
      });
    }
  });
})(window.jQuery, window, document);
