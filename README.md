# Simple Realtime Chat App

## Create sample DB following

### Create DB

```
npx sequelize-cli db:create
```

### Create Table, structure

```
npx sequelize-cli db:migrate
```

### Add sample data

```
npx sequelize-cli db:seed:all
```

### Use these accounts to login 

```
quocnghidev@gmail.com
quocnghidev
```

```
caonam123@gmail.com
caonam123
```

```
longcua2k@gmail.com
longcua2k
```