const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/AuthController');
const ConversationCtrl = require('../controllers/ConvController');
const MessageController = require('../controllers/MessageController');
const UserController = require('../controllers/UserController');

// User login
router.post('/login', AuthController.login);

// Get conversation of a user
router.get('/conversation/:userId', ConversationCtrl.getAll);

// Create new conversation
router.post('/conversation', ConversationCtrl.create);

// Get latest message of a conversation
router.get('/message/:conversationId/latest', MessageController.getLatest);

// Get all messages of a conversation
router.get('/message/:conversationId', MessageController.getAll);

// Add new message
router.post('/message', MessageController.add);

// Get specific user information
router.get('/user/:userId', UserController.getInfo);

module.exports = router;
