const express = require('express');
const router = express.Router();
const SiteController = require('../controllers/SiteController');

router.get('/login', SiteController.login);

router.get('/logout', SiteController.logout);

router.get('/', SiteController.home);

module.exports = router;
