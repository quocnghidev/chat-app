'use strict';

const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Message extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Conversation, User }) {
      this.belongsTo(Conversation, {
        foreignKey: 'conversationId',
        as: 'conversation',
      });

      this.belongsTo(User, {
        foreignKey: 'senderId',
        as: 'sender',
      });
    }
  }
  Message.init(
    {
      text: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Message',
    }
  );
  return Message;
};
