'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Conversation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Message }) {
      this.hasMany(Message, {
        foreignKey: 'conversationId',
        as: 'messages',
      });
    }
  }
  Conversation.init(
    {
      members: {
        type: DataTypes.TEXT,
        get() {
          return JSON.parse(this.getDataValue('members'));
        },
        set(val) {
          this.setDataValue('members', JSON.stringify(val));
        },
      },
    },
    {
      sequelize,
      modelName: 'Conversation',
    }
  );
  return Conversation;
};
